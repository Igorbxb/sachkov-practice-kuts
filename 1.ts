import { writeFileSync } from "fs";
import path from "path";
import { Column, DataType, Model, Sequelize, Table } from "sequelize-typescript";

// Part 1

const filePathForTask1 = path.resolve(__dirname, "output", "output1.json");

const { NUMBER_1, NUMBER_2 } = process.env;

const task1 = () => {
  const number1Parse = Number(NUMBER_1);
  const number2Parse = Number(NUMBER_2);
  if (isNaN(number1Parse)) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: "Агрумент NUMBER_1 не был передан или некорректный" }));
    return;
  }

  if (isNaN(number2Parse)) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: "Агрумент NUMBER_2 не был передан или некорректный" }));
    return;
  }

  const result = Math.pow(number1Parse, number2Parse);

  writeFileSync(
    filePathForTask1,
    JSON.stringify({
      input: {
        NUMBER_1,
        NUMBER_2,
      },
      result,
      message: `Возведение числа ${NUMBER_1} в степень ${NUMBER_2}`,
    })
  );
};

task1();

// Part 2
const filePathForTask2 = path.resolve(__dirname, "output", "output2.json");

const initDB = async () => {
  @Table({})
  class Student extends Model {
    @Column({
      type: DataType.UUID,
      defaultValue: DataType.UUIDV4,
      primaryKey: true,
    })
    declare id: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    surname: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    name: string;

    @Column({
      type: DataType.DATEONLY,
      allowNull: false,
    })
    birthdayDate: Date;
  }

  const sequelize = new Sequelize({
    dialect: "postgres",
    host: "database",
    port: 5432,
    username: "postgres",
    password: "qwerty",
    database: "sachkov_practice_db",
  });

  sequelize.addModels([Student]);

  try {
    await sequelize.authenticate();
    console.log("Init sequelize OK");
  } catch (error) {
    console.error("Init sequelize error", error);
  }

  //Ищем всех студентов у кого есть 2 в оценках
  const studentList = await Student.findAll();

  writeFileSync(filePathForTask2, JSON.stringify(studentList));
};

initDB();
